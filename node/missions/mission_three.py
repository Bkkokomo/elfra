import argparse
import math
import os
import os.path
import subprocess
import time


# TODO: add validation
# TODO: remove erdpy calls and sign and send txs via rpc
amount = math.pow(10, 18) * 1000
proxy = "https://api.elrond.com"
split = 5
text = "Sends 1000 xERD on 5 transaction to the correct address for the passed address shard"

parser = argparse.ArgumentParser(description=text)
parser.add_argument("-V", "--version",
                    help="show program version", action="store_true")
parser.add_argument("--node", "-n", help="node number")
args = parser.parse_args()

path = f"~/elrond/wallets/{args.node}/walletKey.pem"
with open(path) as pem_file:
    for line in pem_file.readlines():
        if '-BEGIN PRIVATE KEY' in line:
            address = line.lstrip('-BEGIN PRIVATE KEY for ').rstrip('-\n')

process = subprocess.Popen(["erdpy", "wallet", "bech32", "--decode", address],
                           stdout=subprocess.PIPE,
                           universal_newlines=True)

hex_address = process.stdout.readline().strip()
print(f"Bech32 Address: {address}")
print(f"Hex Address: {hex_address}")
shard = int(hex_address, 16) % 2
destination_address = ("erd1hqplnafrhnd4zv846wumat2462jy9jkmwxtp3nwmw8ye9eclr6fq40f044",
                       "erd1utftdvycwgl3xt0r44ekncentlxgmhucxfq3jt6cjz0w7h6qjchsjarml6")[shard == 1]

print(f"Shard: {shard}")
print(f"Destination Address: {destination_address}")
print(f"PEM Path: {path}")
print(f"Proxy: {proxy}")

for x in range(split):
    print(f"Amount { str(int(amount / split) / math.pow(10, 18))} xERD")
    process = subprocess.Popen(["erdpy",
                                "tx-prepare-and-send",
                                "--pem",
                                path,
                                "--receiver",
                                destination_address,
                                "--value",
                                str(int(amount / split)),
                                "--proxy",
                                proxy],
                               stdout=subprocess.PIPE,
                               universal_newlines=True)
    res = process.stdout.readline().strip()
    print(f"Result: {res}")
    time.sleep(7)
