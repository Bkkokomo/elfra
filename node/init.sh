#!/bin/bash

SCRIPT_PATH=$(dirname $(realpath -s $0))
cd ${SCRIPT_PATH}

source ~/.env
source ~/node.env
mkdir -p ~/VALIDATOR_KEYS

#TODO: see if we are on AWS before running this
../utilities/create-data-volume.sh

pip3 install erdpy
git clone https://github.com/ElrondNetwork/elrond-go-scripts-v2 &&
	cd elrond-go-scripts-v2 &&
	sed -i "s#CUSTOM_HOME=\"\/home\/ubuntu\"#CUSTOM_HOME=\"${HOME}\"#g" ./config/variables.cfg &&
	sed -i "s/CUSTOM_USER=\"ubuntu\"/CUSTOM_USER=\"${USER}\"/g" ./config/variables.cfg &&
	./script.sh install

# Backup keys
for ((i = 0; i < VALIDATOR_COUNT; i++)); do
	aws s3 cp ~/elrond-nodes/node-${i}/config/validatorKey.pem s3://${BUCKET}/${ENVIRONMENT}/keys/${i}/
done

cd $HOME/elrond-utils &&
	./termui -address localhost:8080
