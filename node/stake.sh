#!/bin/bash

SCRIPT_PATH=$(dirname $(realpath -s $0))
cd ${SCRIPT_PATH}

source ~/.env
source ~/node.env
KEY_NAME="BLS_KEY_"${1}

erdpy --verbose stake \
	--pem=walletKey1.pem \
	--value="2500000000000000000000000" \
	--number-of-nodes=1 \
	--nodes-public-keys="${!KEY_NAME}" \
	--proxy=https://api.elrond.com \
	--estimate-gas
