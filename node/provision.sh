#!/bin/bash

#ssh ${2}@${1} ~/utilities/add-sudoer.sh elrond

scp -r ../utilities/ elrond@${1}:/home/elrond/
scp -r ./ elrond@${1}:/home/elrond/

ssh elrond@${1} ~/node/init.sh