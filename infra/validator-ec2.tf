

resource "aws_instance" "validator" {
  ami                    = data.aws_ami.ubuntu.id
  count                  = var.validator_count[terraform.workspace]
  iam_instance_profile   = aws_iam_instance_profile.profile.name
  instance_type          = var.validator_instance_types[terraform.workspace]
  key_name               = aws_key_pair.auth.id
  subnet_id              = aws_subnet.public.0.id
  vpc_security_group_ids = [aws_security_group.validator.id]
  timeouts {
    create = "30m"
    delete = "10m"
  }
  user_data = templatefile("${abspath(path.root)}/validator-cloud-init.yml", {
    fqdn             = "${var.prefix}-validator-${terraform.workspace}-${count.index + 1}.${var.workspace_domain[terraform.workspace]}"
    prefix           = var.prefix
    ssh_port         = var.ssh_port
    swap_size        = var.swap_size * pow(10, 9)
    validator_number = "${count.index + 1}"
    workspace        = terraform.workspace
  })
  connection {
    type        = "ssh"
    user        = var.prefix
    port        = var.ssh_port
    host        = self.public_ip
    private_key = file(var.private_key_path)
    agent       = false
  }
  root_block_device {
    volume_size = var.validator_root_volume_size
  }
  ebs_block_device {
    device_name = "/dev/sdf"
    snapshot_id = length(data.aws_ebs_snapshot_ids.chaindata.ids) > 0 ? data.aws_ebs_snapshot_ids.chaindata.ids[0] : null
    volume_size = var.validator_data_volume_size
    volume_type = "gp2"
  }
  tags = {
    Name        = "${var.prefix}-validator-${terraform.workspace}-${count.index + 1}"
    environment = terraform.workspace
    group       = var.prefix
    type        = "validator"
  }
  volume_tags = {
    Name        = "${var.prefix}-validator-${terraform.workspace}-${count.index + 1}"
    environment = terraform.workspace
    group       = var.prefix
    type        = "validator"
  }
  provisioner "file" {
    source      = "../config/${terraform.workspace}/.env"
    destination = "/home/${var.prefix}/.env"
  }
  provisioner "file" {
    source      = "../config/${terraform.workspace}/validator/${count.index + 1}/.env"
    destination = "/home/${var.prefix}/node.env"
  }
  provisioner "file" {
    source      = "../node"
    destination = "/home/${var.prefix}/validator"
  }
  provisioner "file" {
    source      = "../utilities"
    destination = "/home/${var.prefix}/utilities"
  }
  provisioner "remote-exec" {
    inline = ["cloud-init status --wait",
      <<EOF
				find ~ -name '*.sh' | xargs  chmod +x
				#./validator/init.sh validator
      EOF
    ]
  }
}
