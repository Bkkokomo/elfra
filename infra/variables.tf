variable "workspace_regions" {
  default = {
    dev   = "us-east-2"
    stage = "eu-west-2"
    prod  = "us-west-2"
  }
}

variable "workspace_domain" {
  default = {
    dev = "foo.bar"
  }
}

variable "workspace_profile" {
  default = {
    default = "default"
    dev     = "default"
    stage   = "default"
  }
}

variable "validator_instance_types" {
  default = {
    dev                 = "m5a.large"
    stage               = "mda.large"
    prod-us-west-2      = "m5n.4xlarge"
    prod-ap-southeast-1 = "m5n.4xlarge"
  }
}

variable "validator_count" {
  default = {
    dev   = 1
    stage = 1
    prod  = 1
  }
}

variable "monitor_instance_types" {
  default = {
    dev                 = "t3a.micro"
    stage               = "t3a.small"
    prod-us-west-2      = "t3a.medium"
    prod-ap-southeast-1 = "t3a.medium"
  }
}

variable "prefix" {
  description = "Name of project being deployed for naming and tagging"
}

variable "ssh_port" {
  description = "sshd daemon listeneng port"
  default     = "22"
}

variable "public_key_path" {
  description = <<DESCRIPTION
Path to the SSH public key to be used for authentication.

Example: ~/.ssh/terraform.pub
DESCRIPTION

  default = "~/.ssh/id_rsa.pub"
}

variable "private_key_path" {
  description = <<DESCRIPTION
Path to the SSH private key to be used for authentication.

Example: ~/.ssh/id_rsa
DESCRIPTION

  default = "~/.ssh/id_rsa"
}

variable "validator_root_volume_size" {
  description = "Desired root volume size in GB"
  default     = "16"
}

variable "validator_data_volume_size" {
  description = "Desired chain data volume size in GB"
  default     = "16"
}

variable "sentry_root_volume_size" {
  description = "Desired root volume size in GB"
  default     = "16"
}

variable "sentry_data_volume_size" {
  description = "Desired chain data volume size in GB"
  default     = "16"
}

variable "monitor_root_volume_size" {
  description = "Desired root volume size in GB"
  default     = "16"
}

variable "monitor_data_volume_size" {
  description = "Desired data volume size in GB"
  default     = "2"
}

variable "swap_size" {
  description = "Desired swap file size in GB"
  default     = "2"
}

variable "ubuntu_account_number" {
  description = "AMI owner ID"
  default     = "099720109477"
}

variable "data_snapshot_name" {
  description = "Chain data snapshot name"
}

variable "ip_whitelist" {
  description = "List of ip/cidr to be whitelisted for each ec2 instance"
}

