output "validator-ip" {
  value = aws_instance.validator.*.public_ip
}
output "monitor-ip" {
  value = aws_instance.monitor.public_ip
}
output "validator-id" {
  value = aws_instance.validator.*.id
}
output "monitor-id" {
  value = aws_instance.monitor.id
}
output "prefix" {
  value = var.prefix
}