#!/bin/bash

source ../utilities/rainbow.sh

if [[ "${2}" != "" ]]; then
	terraform output ${1}-ip | sed ':a;N;$!ba;s/,\n]/]/g' | jq -r ".[${2}]"
	ssh -o "StrictHostKeyChecking no" $(terraform output prefix)@$(terraform output ${1}-ip | sed ':a;N;$!ba;s/,\n]/]/g' | jq -r ".[${2}]")
elif [[ ! -z "${1}" ]]; then
	ssh -o "StrictHostKeyChecking no" $(terraform output prefix)@$(terraform output ${1}-ip)
else
	echored "Missing ssh target instance!"
fi

#aws ssm start-session --target $(terraform output ${1}-id)
