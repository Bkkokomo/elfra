# Elrond Blockchain

## Getting Started

### Example usage

This example will create the following resources in the specified AWS Region:

-   Virtual Private Cloud (10.0.0.0/16)
-   Internet Gateway
-   Route Table
    -   Egress all
-   Public Subnet (10.0.1.0/24) Zone 1
-   Elastic Load Balancer (Monitor)
    -   LB https 443
    -   Monitor http 3000 (grafana)
-   Security Group (ELB)
    -   Ingress
        -   443 tcp all
    -   Egress
        -   all all all
-   Security Group (elrond-node)
    -   Ingress
        -   22 tcp all
        -   8545 tcp monitor (elrond rpc)
        -   30303 udp all (elrond p2p) - 30303 udp all (elrond p2p)
    -   Egress
        -   all all all
-   Key Pair
-   EC2 Instances (Ubuntu Server 20.04 LTS (HVM), SSD Volume Type) - Validator - Sentry - Monitor

After you run `terraform apply` on this configuration, it will
autoelrondally output the following:

-   Monitor IP address
-   Monitor instance ID
-   Validator IP address
-   Vaidator instance ID
-   Sentry IP address
-   Sentry instance ID
-   Project prefix

For example:

```
terraform apply \
   -var 'key_name=terraform' \
   -var 'swap-size=2' \
   -var 'volume-size=32' \
   -var 'private_key_path=~/.ssh/id_rsa.pub' \
   -var 'public_key_path=~/.ssh/id_rsa.pub'
```

See `variables.tf` for a list of all available variables.
