data "aws_route53_zone" "root" {
  name         = var.workspace_domain[terraform.workspace]
  private_zone = false
}

resource "aws_route53_record" "monitor" {
  zone_id = data.aws_route53_zone.root.zone_id
  name    = "${var.prefix}-monitor-${terraform.workspace}.${var.workspace_domain[terraform.workspace]}"
  type    = "A"
  alias {
    name                   = aws_elb.monitor.dns_name
    zone_id                = aws_elb.monitor.zone_id
    evaluate_target_health = true
  }
}
