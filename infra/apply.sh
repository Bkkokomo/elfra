#!/bin/bash

terraform fmt
terraform validate

if [[ "$?" == "0" ]]; then

	terraform apply -auto-approve

	if [[ "$?" == "0" ]]; then
		exec ./connect.sh validator 0
	fi
fi
