#!/bin/bash

SCRIPT_PATH=$(dirname $(realpath -s $0))
cd ${SCRIPT_PATH}

source ../config/${1}/.env

scp -P ${SSH_PORT} -r ../node/ ${USR}@${HOST}:/home/${USR}/elrond/