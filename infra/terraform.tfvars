data_snapshot_name = "chaindata"
ip_whitelist       = ["152.231.0.0/16", "186.15.0.0/16", "47.205.0.0/16"]
prefix             = "elrond"
swap_size          = 2
workspace_domain = {
  dev = "alphavirtual.com"
}
workspace_profile = {
  dev = "default"
}
