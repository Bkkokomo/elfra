#!/bin/bash

SCRIPT_PATH=$(dirname $(realpath -s $0))
source ${SCRIPT_PATH}/../.env
source ${SCRIPT_PATH}/../utilities/rainbow.sh
${SCRIPT_PATH}/../utilities/create-data-volume.sh

mkdir -p /data/prometheus
chmod -R ugo+rw /data/

cd ~/monitor &&
	git clone https://gitlab.com/elrond-av/matmon.git &&
	HOSTNAME=${HOSTNAME} docker-compose up -d &&
	tail /var/log/cloud-init-output.log
