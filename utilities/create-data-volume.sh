#!/bin/bash

sudo file -s /dev/nvme1n1 | grep "/dev/nvme1n1: data" &&
	sudo mkfs -t xfs /dev/nvme1n1 &&
	sudo mount /dev/nvme1n1 /data &&
	sudo chown -R ${USER}:${USER} /data &&
	mkdir -p /data/heimdall/data &&
	mkdir -p /data/heimdall/config &&
	mkdir -p bor/dataDir/keystore/ &&
	chmod -R ugo+rw /data/