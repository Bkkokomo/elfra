#!/bin/bash

*filter
:INPUT ACCEPT [0:0]
:FORWARD ACCEPT [0:0]
:OUTPUT ACCEPT [0:0]
:fail2ban-ssh - [0:0]
-A INPUT -m state --state RELATED,ESTABLISHED -j ACCEPT 
-A INPUT -s 152.231.129.0/24 -j ACCEPT  
-A INPUT -p tcp -m multiport --dports 22 -j fail2ban-ssh 
-A INPUT -i lo -j ACCEPT 
-A INPUT -d 127.0.0.0/8 ! -i lo -j REJECT --reject-with icmp-port-unreachable 
-A INPUT -m state --state NEW -p tcp --dport 22 -j ACCEPT
-A INPUT -m state --state NEW -p tcp --dport 80 -j ACCEPT
-A INPUT -m state --state NEW -p tcp --dport 443 -j ACCEPT
-A INPUT -p icmp -m icmp --icmp-type 8 -j ACCEPT 
-A INPUT -m limit --limit 5/min -j LOG --log-prefix "iptables denied: " --log-level 7 
-A INPUT -j REJECT --reject-with icmp-port-unreachable 
-A FORWARD -j REJECT --reject-with icmp-port-unreachable 
-A OUTPUT -j ACCEPT 
-A fail2ban-ssh -j RETURN 
COMMIT
