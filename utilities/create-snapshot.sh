#!/bin/bash

SCRIPT_PATH=$(dirname $(realpath -s $0))
source ${HOME}/.env
source ${SCRIPT_PATH}/rainbow.sh

INSTANCE_ID=$(ec2metadata --instance-id)
REGION=$(ec2metadata --availability-zone | sed 's/.$//')

if [[ -z ${INSTANCE_ID} ]]; then
	echo "Could not find instance-id: make sure cloud-utils package is installed"
	exit 1
fi

if [[ -z ${REGION} ]]; then
	echo "Could not find instance region"
	exit 1
fi

SNAPSHOT_ID=$(
	aws ec2 describe-snapshots \
		--region ${REGION} \
		--filters Name=description,Values=${1} |
		jq -r '.Snapshots[].SnapshotId'
)

VOLUME_ID=$(
	aws ec2 describe-volumes \
		--region ${REGION} --filters Name=attachment.instance-id,Values=${INSTANCE_ID} |
		jq -r '.Volumes[].Attachments[]	| select(.Device == "/dev/sdf").VolumeId'
)

if [[ -z ${VOLUME_ID} ]]; then
	echo "Could not find blockchain data volume"
	exit 1
fi

aws ec2 create-snapshot \
	--region ${REGION} \
	--volume-id ${VOLUME_ID} \
	--description ${1} \
	--tag-specifications \
	"ResourceType=snapshot,Tags=[{Key=Name,Value=${1}},{Key=group,Value=${SNAPSHOT_GROUP}}]"

if [[ ! -z ${SNAPSHOT_ID} ]]; then
	aws ec2 delete-snapshot --snapshot-id ${SNAPSHOT_ID} --region ${REGION}
fi
